import random

__botname__ = "TBA: Test Bot Annihilator"
__entryfunction__ = "tba"


def tba(opponent=None, mine=None):
    if len(opponent) == 0:
        return random.randrange(0, 3)
    else:
        to_beat = opponent[-1]

    if to_beat == 0:
        return 1
    elif to_beat == 1:
        return 2
    elif to_beat == 2:
        return 0
